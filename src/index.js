import React from 'react'
import ReactDOM from 'react-dom'

import './styles/main.scss'

import Application from './application'

const root = document.querySelector('#root')

ReactDOM.render(React.createElement(Application), root)
