import React from 'react'

import messages from '../../../data/messages'

import Message from './message'

import './style.scss'
import PhoneSVG from '../../../svg/phone.svg'

class MessagesList extends React.PureComponent {
  renderMessages () {
    return messages.map((m, index, all) => {
      return <Message key={index} message={m} withUsername={index === 0 || all[index - 1].direction !== m.direction} />
    })
  }

  render () {
    return <section className='messages-list'>
      <PhoneSVG width={200} className='phone' />
      {this.renderMessages()}
      <div className='input'><span>Сервис отправки сообщений временно недоступен</span></div>
      <div className='slider' />
    </section>
  }
}

export default MessagesList
