import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import directionsType from '../../../data/directions-type'

class Message extends React.PureComponent {
  getUsername () {
    return this.props.message.direction === directionsType.FROM ? 'Артем Шульга' : 'Вы'
  }

  getClassNames () {
    return classnames([
      'message',
      this.props.message.direction === directionsType.FROM ? 'direction-from' : 'direction-to',
      this.props.message.emoji ? 'emoji' : '',
      this.props.withUsername ? 'withUsername' : ''
    ])
  }

  getMessage () {
    let content
    if (this.props.message.link) {
      content = <a href={this.props.message.message} target='_blank' rel='noopener noreferrer'>{this.props.message.message}</a>
    } else {
      content = this.props.message.message
    }
    return <div className='message-cloud'>{content}</div>
  }

  renderUsername () {
    if (this.props.withUsername) {
      return <div className='username'>{this.getUsername()}</div>
    } else {
      return null
    }
  }

  render () {
    return <div className={this.getClassNames()}>
      {this.renderUsername()}
      {this.getMessage()}
    </div>
  }
}

Message.propTypes = {
  message: PropTypes.object,
  withUsername: PropTypes.bool
}

export default Message
