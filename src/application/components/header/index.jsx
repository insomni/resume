import React from 'react'

import './style.scss'

import userpic from '../../../img/userpic.jpg'

class Header extends React.PureComponent {
  render () {
    return <header className='header'>
      <div className='buttons'>
        <div className='button close' />
        <div className='button expand' />
        <div className='button turn' />
      </div>
      <div className='profile'>
        <div className='userpic'><img src={userpic} width={130} height={130} /></div>
        <div className='title'>Front-end developer</div>
        <div className='subtitle'>Шульга Артем Александрович</div>
      </div>
    </header>
  }
}

export default Header
