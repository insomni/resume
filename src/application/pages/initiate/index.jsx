import React from 'react'

import Header from '../../components/header'
import MessagesList from '../../components/messages-list'

import './style.scss'
import GitlabSVG from '../../../svg/gitlab-icon-rgb.svg'

const SCROLL_TO_ACTION = 100

class InitiatePage extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      scroll: window.pageYOffset
    }

    this.handleScroll = this.handleScroll.bind(this)
  }

  componentDidMount () {
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll)
  }

  getMessagesStyles () {
    return {
      opacity: this.state.scroll > SCROLL_TO_ACTION ? 1 : 0.5,
      transform: this.state.scroll > SCROLL_TO_ACTION ? null : 'scale(0.9)'
    }
  }

  getHeaderStyles () {
    return {
      transform: this.state.scroll > SCROLL_TO_ACTION ? 'translateY(-150px)' : null
    }
  }

  handleScroll (e) {
    this.setState({ scroll: window.pageYOffset })
  }

  renderCopyright () {
    return <div className='copyright'>
      <span>Код приложения на</span>
      <a href='https://gitlab.com/insomni/resume' target='_blank' rel='noopener noreferrer'>
        <GitlabSVG className='gitlab-logo' height={32} />
      </a>
    </div>
  }

  render () {
    return <div className='wrapper'>
      <div className='page initiate'>
        <div className='header' style={this.getHeaderStyles()}>
          <Header />
        </div>
        <div className='messages' style={this.getMessagesStyles()}>
          <MessagesList />
        </div>
        {this.renderCopyright()}
      </div>
    </div>
  }
}

export default InitiatePage
