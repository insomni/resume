import React from 'react'

import InitiatePage from './pages/initiate'

class Application extends React.PureComponent {
  render () {
    return <InitiatePage />
  }
}

export default Application
