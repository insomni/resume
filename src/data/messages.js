import moment from 'moment'

import directionTypes from './directions-type'

const time = moment().hours()

export default [
  {
    message: 'Привет 😊',
    direction: directionTypes.TO
  },
  {
    message: 'Привет!',
    direction: directionTypes.FROM
  },
  {
    message: '👋',
    emoji: true,
    direction: directionTypes.FROM
  },
  {
    message: 'Расскажи о себе',
    direction: directionTypes.TO
  },
  {
    message: 'Меня зовут Артем и я front-end разработчик 🚁',
    direction: directionTypes.FROM
  },
  {
    message: 'В процессе учебы и работы сталкиваюсь со сложными и интереснными задачами, для решения которых использую Javascript и Python 3',
    direction: directionTypes.FROM
  },
  {
    message: 'Я постоянно учусь и интересуюсь новыми технологиями',
    direction: directionTypes.FROM
  },
  {
    message: 'Как с тобой связаться?',
    direction: directionTypes.TO
  },
  {
    message: 'Пиши в вк или телеграм 😉',
    direction: directionTypes.FROM
  },
  {
    message: 'https://vk.com/cruperman',
    link: true,
    direction: directionTypes.FROM
  },
  {
    message: 'http://t.me/cruperman',
    link: true,
    direction: directionTypes.FROM
  },
  {
    message: '👌',
    emoji: true,
    direction: directionTypes.TO
  },
  {
    message: time < 22 && time >= 17 ? 'Приятного вечера 🍸' : time < 17 && time >= 7 ? 'Приятного дня ☕' : 'Доброй ночи 🌠',
    direction: directionTypes.FROM
  }
]
